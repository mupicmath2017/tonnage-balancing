# README #

### What is this repository for? ###

## Quick summary

The city of Huntington, WV maintains a fleet of six garbage trucks responsible for collecting the city's garbage.  These six trucks must service every municipal customer during a week.  Recent demographic shifts in the city have caused the quantity of garbage collected by each truck to be uneven.  The city desires that each truck collect approximately the same amount of garbage on each weekday.  This repository contains Python code that searches possible route modifications for the trucks in an attempt to improve the collection routes.

Version: 5/3/17

### How do I get set up? ###

You'll need to have Python 3.x installed.  You can get it here:
https://www.python.org/downloads/

Make sure to tick the box to add Python to your system PATH.

If the installer didn't do it for you, you can add python to your PATH manually.  The easiest way to do this is to use Windows graphical interface:
* Open an explorer window, right-click "This PC", and click "Properties"
* Go to Advanced System Settings->Advanced->Environment Variables...
* Select the PATH variable and hit the "Edit" button
* Add the Python base directory (usually C:\Python36) and the Python36\Scripts directory to your PATH and save your changes

This project has no external dependencies and can thus be run with vanilla Python outside a virtual environment.  (Haggard Python contributors often fall in love with us after reading this sentence.)


### How does it work? ###

The script attempts to spread tonnage collection evenly across trucks for a given day by swapping blocks between routes.  The amount of garbage contributed by a route is measured by either a raw tonnage prediction or a "tonnage score" representing deviation from average production.

The script reads a CSV file from the base directory of the project which contains info on the set of customers serviced by the city along with socio-economic and demographic factors for each customer pulled from American Fact Finder and the Cabell County assessor.  Each customer is associated with a city block and their current garbage route.

Customers are grouped into municipal blocks using the BLOCKID field in the customer list.  Blocks are then grouped into Routes based on which truck collects garbage for the route.  Routes are grouped into a RouteSet object, which can be evaluated for fitness.

The swapping algorithm works as follows:

* Consider every block on the border of two routes
* Ask what would happen if we swapped the block from its current route to a neighboring route?
* Evaluate the swap with a fitness function
* Keep the best swap found
* Repeat until no good swap is found

The output of the script is a csv file containing each Block ID for a given weekday along with the suggested new route.

### Who do I talk to? ###

Have questions?
You can contact the development team with questions:

* Zach Jones - jones867@marshall.edu
* Steven Rollins - rollins51@marshall.edu
* David Hannan - hannan6@marshall.edu

This project was sponsored by Dr. Michael Schroeder, who heads up the Community-Oriented Research and Training Experience (CORTEx) program at Marshall University.  He can be contacted at:

* schroederm@marshall.edu