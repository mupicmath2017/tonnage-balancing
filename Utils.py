import csv
from functools import reduce

from classes.Customer import Customer


# reads customer list specified by filename parameter and creates Python Customer objects for each customer
# the day parameter allows restriction of the customer list to a single day, or all days if 'A' is passed
def read_customers(filename, day='A'):
    # mappings from logical names to column positions in CSV file being read
    # The order of the keys in the columns MUST match the order of the parameters declared in the Customer constructor
    customer_list_columns = {
        "account": 2,
        "name": 3,
        "parcel": 4,
        "block": 5,
        "route": 6,
        "neighbors": 7,
        "hs_grads": 8,
        "cg_grads": 9,
        "home_val": 10,
        "home_sa": 11,
        "hh_size": 12,
        "income": 13
    }
    customers = []
    with open(filename, 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        header = next(reader, False)

        row = next(reader, False)
        i = 0
        while row:
            # create a customer from the row
            # the asterisk is the "splat" operator which unpacks a list to function arguments
            customer = Customer(
                * map(lambda index: row[index], customer_list_columns.values())
            )
            if customer.route and (day == 'A' or day == customer.route[0]):
                customers.append(customer)
            row = next(reader, False)
            i += 1

    return customers


# sums up a list of numbers
def sum(num_list):
    return reduce(lambda x, y: x + y, num_list)


# computes the average of a list of numbers
def mean(num_list):
    return sum(num_list) / float(len(num_list))


# computes the sum of squared differences for a list of numbers
def squared_diff(num_list):
    list_mean = mean(num_list)
    distances = list(map(lambda x: (x-list_mean)**2, num_list))
    return sum(distances)


# computes the square of the std deviation for a list of numbers
def sigma_squared(num_list):
    return squared_diff(num_list) / float(len(num_list))

