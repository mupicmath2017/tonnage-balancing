"""
    This script was designed to run in QGIS to create an animation of route swaps for a given day
    Replace the the fully-qualified path to the Animation_Before and Animation_Swaps csv files with paths relative to your own system

    author: Zach Jones - jones867@marshall.edu
    version: 5/2/2017
"""

from qgis.utils import iface
from qgis.core import *
from PyQt4.QtCore import QVariant
from PyQt4 import QtCore
import csv
import time


def draw_layers():

    # print("Starting!")

    renderer = None

    day = 'R'
    mode = '2'
    blocks = {}
    with open("C:\\Users\\Zach Jones\\Documents\\Develop\\tonnage-balancing\\output\\animation\\Animation_Before_"
                      + day + "_mode_" + mode + ".csv", 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        row = next(reader, False)
        while row:
            blocks[row[0]] = row[1]
            row = next(reader, False)

    # draw starting colors
    truck_colors = {
        "23": '#ff9209',  # orange
        "24": '#3e26b7',  # blue
        "25": '#fbff01',  # yellow
        "26": '#f6a0db',  # pink
        "27": '#09d832',  # green
        "29": '#ed1729',  # red
    }
    layer = None
    for lyr in QgsMapLayerRegistry.instance().mapLayers().values():
        if lyr.name() == "animation_visualization_blocks":
            layer = lyr
    fni = layer.fieldNameIndex('GEOID10')
    unique_values = layer.dataProvider().uniqueValues(fni)
    categories = {}

    for unique_value in blocks:
        route = blocks[unique_value].strip()
        truck_no = route[1:]
        color = '#6b6b6b'  # default color is dark gray
        if truck_no in truck_colors:
            color = truck_colors[truck_no]

        # initialize the default symbol for this geometry type
        symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())

        # configure a symbol layer
        layer_style = {}
        layer_style['color'] = color
        layer_style['outline'] = '#000000'
        symbol_layer = QgsSimpleFillSymbolLayerV2.create(layer_style)

        # replace default symbol layer with the configured one
        if symbol_layer is not None:
            symbol.changeSymbolLayer(0, symbol_layer)

        # create renderer object
        category = QgsRendererCategoryV2(unique_value, symbol, str(unique_value))
        # entry for the list of category items
        categories[unique_value] = category

    # default renderer paints everything gray
    color = '#6b6b6b'  # default color is dark gray

    # initialize the default symbol for this geometry type
    symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())

    # configure a symbol layer
    layer_style = {}
    layer_style['color'] = color
    layer_style['outline'] = '#000000'
    symbol_layer = QgsSimpleFillSymbolLayerV2.create(layer_style)

    # replace default symbol layer with the configured one
    if symbol_layer is not None:
        symbol.changeSymbolLayer(0, symbol_layer)

    # create renderer object
    category = QgsRendererCategoryV2("", symbol, str(""))
    # entry for the list of category items
    categories[""] = category

    # create renderer object
    renderer = QgsCategorizedSymbolRendererV2('GEOID10', list(categories.values()))

    # assign the created renderer to the layer
    if renderer is not None:
        layer.setRendererV2(renderer)

    layer.triggerRepaint()

    # # print("Colored original map...")
    time.sleep(2.5)


    # iterate over each swap that was made:
    with open("C:\\Users\\Zach Jones\\Documents\\Develop\\tonnage-balancing\\output\\animation\\Animation_Swaps_"
                      + day + "_mode_" + mode + ".csv", 'rt') as csvfile:
        reader = csv.reader(csvfile, delimiter=',')

        row = next(reader, False)
        i = 0
        while row:
            block_id = row[0]
            new_route = row[1].strip()
            new_truck_no = new_route[1:]

            # # print("Moving block %s from route %s to route %s" % (block_id, blocks[block_id], new_route))

            new_color = truck_colors[new_truck_no]

            # initialize the default symbol for this geometry type
            symbol = QgsSymbolV2.defaultSymbol(layer.geometryType())

            # configure a symbol layer
            layer_style = {}
            layer_style['color'] = new_color
            layer_style['outline'] = '#000000'
            symbol_layer = QgsSimpleFillSymbolLayerV2.create(layer_style)

            # replace default symbol layer with the configured one
            if symbol_layer is not None:
                symbol.changeSymbolLayer(0, symbol_layer)

            category_index = renderer.categoryIndexForValue(block_id)
            renderer.updateCategorySymbol(category_index, symbol)

            layer.triggerRepaint()

            time.sleep(0.3)

            row = next(reader, False)
            i += 1


class Worker(QtCore.QObject):
    def __init__(self, *args, **kwargs):
        QtCore.QObject.__init__(self, *args, **kwargs)
        self.abort = False

    def run(self):
        try:
            draw_layers()
            print("Task finished")
            self.finished.emit(True)
        except:
            import traceback
            print(traceback.format_stack())
            # self.error.emit(traceback.format_exc())
            self.finished.emit(False)


    def kill(self):
        self.abort = True

    finished = QtCore.pyqtSignal(bool)

thread = QtCore.QThread()
worker = Worker()
worker.moveToThread(thread)
thread.started.connect(worker.run)
worker.finished.connect(worker.deleteLater)
thread.finished.connect(thread.deleteLater)
worker.finished.connect(thread.quit)
thread.start()
