class Block(object):
    def __init__(self, id, route):
        self.id = id
        self.route = route
        self.day = route[0]  # day will be one of MTWRF
        self.customers = []
        self.earnings = 0
        self.home_val = 0
        self.aggregate_hh_size = 0
        self.garbage_production = 0  # this needs to calculated later by calling compute_garbage_production()
        self.tonnage_score = 0  # this needs to calculated later by calling compute_tonnage_score()

    # adds a customer to the block
    def add_customer(self, customer):
        self.customers.append(customer)
        self.earnings += customer.income
        self.home_val += customer.home_val
        self.aggregate_hh_size += customer.hh_size

        # determine which route this block best fits
        customer_routes = {}
        for customer in self.customers:
            route = customer.route
            if route in customer_routes:
                customer_routes[route] += 1
            else:
                customer_routes[route] = 1

        route_with_most_customers = None
        most_customers = 0
        for route in customer_routes:
            if customer_routes[route] > most_customers:
                most_customers = customer_routes[route]
                route_with_most_customers = route

        self.route = route_with_most_customers

    # removes a customer from the block - this virtually never happens
    def remove_customer(self, customer):
        self.customers.remove(customer)
        self.earnings -= customer.income
        self.home_val -= customer.home_val
        self.aggregate_hh_size -= customer.hh_size

    # sets the route id of all the customers in this block (used for swapping)
    def set_route(self, route):
        self.route = route
        for customer in self.customers:
            customer.set_route(route)

    # gets blocks adjacent to this block that are collected on the same day
    def get_adjacent_blocks(self, otherblocks):
        adjacent_blocks = set()
        for customer in self.customers:
            customer_blocks = customer.neighbors.split(':')
            for block_id in customer_blocks:
                if block_id in otherblocks:
                    block = otherblocks[block_id]
                    adjacent_blocks.add(block)

        return adjacent_blocks

    # a blocks tonnage score is intended to represent how much garbage it produces relative to other blocks
    # a tonnage score of zero (before the modifier) means it is perfectly average
    def compute_tonnage_score(self, mean_earnings, sd_earnings, mean_home_val, sd_home_val, mean_hh_size, sd_hh_size, modifier=1.4):

        # modifier shifts tonnage score so that it is always positive

        std_earnings = (self.earnings - mean_earnings) / sd_earnings

        std_home_val = (self.home_val - mean_home_val) / sd_home_val

        std_hh_size = (self.aggregate_hh_size - mean_hh_size) / sd_hh_size

        self.tonnage_score = .590509 * std_earnings - .564037 * std_home_val + .471836 * std_hh_size

        return self.tonnage_score + modifier

    # computes estimated tonnage produced by this block
    # model was created using multiple regression through the origin on route-level tonnage measurements
    def compute_garbage_production(self):

        self.garbage_production = (1.661e-07*self.earnings)-(3.898e-08*self.home_val)+(7.893e-03*self.aggregate_hh_size)

        return self.garbage_production

    # returns the number of customers in this block
    def get_customer_count(self):
        return len(self.customers)

    # outputs all customers in this block as csv lines
    def to_csv(self):
        return "\n".join(customer.to_csv() for customer in self.customers)

    # outputs the id of this block with its route
    def to_block_csv(self):
        return "{}, {}".format(self.id, self.route)
