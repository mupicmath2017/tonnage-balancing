from Utils import sigma_squared


class RouteSet(object):
    # these 5 modes are supported for evaluating fitness.  See compute_fitness function for full details
    FITNESS_MODE_PREDICTED_TONNAGE = 1
    FITNESS_MODE_TONNAGE_SCORE = 2
    FITNESS_MODE_CUSTOMER_COUNT = 3
    FITNESS_MODE_TONNAGE_SCORE_CUSTOMER_COUNT = 4
    FITNESS_MODE_PREDICTED_TONNAGE_CUSTOMER_COUNT = 5
    def __init__(self):
        self.routes = []

    def add_route(self, route):
        self.routes.append(route)

    # swaps unit from its current route to the specified route
    def swap_block(self, block, to_route):
        # find the route to which the unit currently belongs
        # this line can be read "find the next route in the routeset such that the route has an id matching unit.route"
        current_route = next(route for route in self.routes if route.id == block.route)

        # remove unit from its current route
        current_route.remove_block(block)

        # set the new route id
        block.set_route(to_route.id)

        # add unit to its new route
        to_route.add_block(block)

    # computes the fitness score of this routeset
    # despite the counterintuitive name, our goal is to minimize this function
    # the mode parameter should be one of the static class variables that defines the method used to compute fitness
    def compute_fitness(self, mode=None):
        if mode == RouteSet.FITNESS_MODE_PREDICTED_TONNAGE:
            # this mode evaluates fitness by computing the standard deviation in predicted tonnages collected by a route
            predicted_tonnages = []
            for route in self.routes:
                predicted_tonnages.append(route.compute_garbage_production())

            return sigma_squared(predicted_tonnages) ** 0.5

        elif mode == RouteSet.FITNESS_MODE_TONNAGE_SCORE:
            # this mode evaluates fitness by computing the standard deviation of the aggregate tonnage score for each route
            predicted_tonnage_scores = []
            for route in self.routes:
                predicted_tonnage_scores.append(route.compute_tonnage_score())

            return sigma_squared(predicted_tonnage_scores) ** 0.5

        elif mode == RouteSet.FITNESS_MODE_CUSTOMER_COUNT:
            # this mode evaluates fitness by computing the standard deviation of the # of customers collected by each route
            customer_counts = []
            for route in self.routes:
                customer_counts.append(route.compute_customer_count())

            return sigma_squared(customer_counts) ** 0.5

        elif mode == RouteSet.FITNESS_MODE_TONNAGE_SCORE_CUSTOMER_COUNT:
            # this mode evaluates fitness by a weighted product of std dev in tonnage score and std deviation in # customers
            customer_counts = []
            for route in self.routes:
                customer_counts.append(route.compute_customer_count())

            tonnage_score_sums = []
            for route in self.routes:
                tonnage_score_sums.append(route.compute_tonnage_score())

            sigma_c = sigma_squared(customer_counts) ** 0.5

            sigma_t = sigma_squared(tonnage_score_sums) ** 0.5

            # you can change the powers x,y in sigma_c**x and sigma_t**y to get different weightings.  enforce x+y = 1
            return (sigma_c**0.5) * (sigma_t**0.5)

        elif mode == RouteSet.FITNESS_MODE_PREDICTED_TONNAGE_CUSTOMER_COUNT:
            # this mode evaluates fitness by a weighted product of std dev in predicted tonnage collected and std deviation in # customers
            customer_counts = []
            for route in self.routes:
                customer_counts.append(route.compute_customer_count())

            predicted_tonnages = []
            for route in self.routes:
                predicted_tonnages.append(route.compute_garbage_production())

            sigma_c = sigma_squared(customer_counts) ** 0.5

            sigma_t = sigma_squared(predicted_tonnages) ** 0.5

            # you can change the powers x,y in sigma_c**x and sigma_t**y to get different weightings.  enforce x+y = 1
            return (sigma_c ** 0.5) * (sigma_t ** 0.5)
        else:
            print("WARNING: unsupported or unknown mode for evaluating fitness")
            return -1

    # finds all the routes for the given day;  day should be one of MTWRF
    def get_routes_for_day(self, day):
        return list(filter(lambda x: x.day == day, self.routes))

    # finds the route with the given id
    def get_route_by_id(self, id):
        return next(route for route in self.routes if route.id == id)

    # gets a csv string representing the customer list
    def to_csv(self):
        header = "AccountNum,CustomerNa,Parcel_ID,p_BLOCKID,p_ROUTEID,b_NEIGHBOR,HS_GRADS,CG_GRADS,APR_VAL,SFLA,HH_Size,AVG_EARN,GARBAGE_OUT\n"
        return header + "\n".join(route.to_csv() for route in self.routes)

    def blocks_to_csv(self):
        header = "BlockID, Route\n"
        return header + "\n".join(route.csv_of_blocks() for route in self.routes)
