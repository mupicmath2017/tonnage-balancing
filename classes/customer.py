class Customer(object):
    def __init__(self, account, name, parcel, block, route, neighbors, hs_grads, cg_grads, home_val, home_sa, hh_size,
                 income):
        self.id = account
        self.name = name
        self.parcel = parcel
        self.block = block
        self.route = route
        self.neighbors = neighbors
        self.hs_grads = float(hs_grads) if len(hs_grads.strip()) else 0
        self.cg_grads = float(cg_grads) if len(cg_grads.strip()) else 0
        self.home_val = float(home_val) if len(home_val.strip()) else 0
        self.home_sa = float(home_sa) if len(home_sa.strip()) else 0
        self.hh_size = float(hh_size) if len(hh_size.strip()) else 0
        self.income = float(income) if len(income.strip()) else 0

        self.garbage = None

    def set_route(self, route):
        self.route = route

    def to_csv(self):
        return str.format("{}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}, {}",
                          self.id, self.name, self.parcel, self.block, self.route, self.neighbors, self.hs_grads,
                          self.cg_grads, self.home_val, self.home_sa, self.hh_size, self.income, self.garbage)
