import Utils


class Route(object):
    def __init__(self, route_id):
        self.blocks = dict()
        self.id = route_id
        self.day = route_id[0]  # MTWRF for weekday

    # Sum up garbage production in this route
    def compute_garbage_production(self):
        total = 0
        for block in self.blocks.values():
            total += block.compute_garbage_production()

        return total

    # Sum up tonnage scores for blocks in this route
    # note that this is NOT a good indicator of garbage production!
    def compute_tonnage_score(self, modifier=1.4):
        block_earnings = []
        block_home_vals = []
        block_hh_sizes = []
        for block in self.blocks.values():
            block_earnings.append(block.earnings)
            block_home_vals.append(block.home_val)
            block_hh_sizes.append(block.aggregate_hh_size)

        mean_earnings = Utils.mean(block_earnings)
        sd_earnings = Utils.sigma_squared(block_earnings) ** 0.5
        mean_home_val = Utils.mean(block_home_vals)
        sd_home_val = Utils.sigma_squared(block_home_vals) ** 0.5
        mean_hh_size = Utils.mean(block_hh_sizes)
        sd_hh_size = Utils.sigma_squared(block_hh_sizes) ** 0.5

        total = 0
        for block in self.blocks.values():
            total += block.compute_tonnage_score(mean_earnings, sd_earnings, mean_home_val, sd_home_val, mean_hh_size, sd_hh_size, modifier)

        return total

    def compute_customer_count(self):
        total = 0
        for block in self.blocks.values():
            total += block.get_customer_count()

        return total

    def add_block(self, block):
        self.blocks[block.id] = block

    def remove_block(self, block):
        del self.blocks[block.id]

    def to_csv(self):
        return "\n".join(block.to_csv() for block in self.blocks.values())

    def csv_of_blocks(self):
        return "\n".join(block.to_block_csv() for block in self.blocks.values())

    # returns the number of disconnected components in the current route by doing a BFS on each part
    def count_components(self, all_blocks):
        visited_blocks = set()
        num_components = 0
        while len(visited_blocks) < len(list(self.blocks.keys())):
            num_components += 1
            # find a unit that hasn't been visited and do a BFS
            unvisited = next(block for block in self.blocks.values() if block.id not in visited_blocks)
            nodes = self.bfs(unvisited, all_blocks)
            visited_blocks.update(nodes)

        return num_components

    # does a breadth-first search of blocks in this route starting with the specified block
    def bfs(self, start_block, all_blocks):
        visited, queue = set(), [start_block.id]
        while queue:
            vertex = queue.pop(0)
            if vertex in self.blocks and vertex not in visited:
                visited.add(vertex)
                adjacent_blocks = self.blocks[vertex].get_adjacent_blocks(all_blocks)
                adjacent_block_ids = set(map(lambda x: x.id, adjacent_blocks))
                # adjacent_units = filter(lambda x: x.route == start_block.route, self.blocks[vertex].get_adjacent_blocks())

                queue.extend(set(adjacent_block_ids - visited))

        return visited
