from Utils import *
from classes.Route import Route
from classes.RouteSet import RouteSet
from classes.Block import Block

# configuration variables
verbose = True  # verbose mode will print more output to the console
day = 'F'  # customers read will be restricted to this day - should be one of MTWRF
fitness_mode = 5  # fitness mode with which to evaluate a route - see RouteSet.py for more details
maxIt = 100  # maximum number of iterations before the optimization routine terminates

if verbose: print("Reading customer list...")
customers = read_customers("customer_list_example.csv", day)
if verbose: print("Customers read")

# read customers into blocks
if verbose: print("Creating city blocks")
blocks = {}
for customer in customers:
    # only add customers on the routes
    if customer.route and len(customer.route) > 0:
        key = customer.block
        if key in blocks:
            blocks[key].add_customer(customer)
        else:
            day = customer.route[0]
            block = Block(key, customer.route)
            block.add_customer(customer)
            blocks[key] = block

# read blocks into routes
if verbose: print("Creating initial routes")
routes = {}
for block in blocks.values():
    route_no = block.route
    if route_no not in routes:
        routes[route_no] = Route(route_no)
    routes[route_no].add_block(block)

# read routes into a routeset
if verbose: print("Creating RouteSet object")
routeset = RouteSet()
for route in routes.values():
    routeset.add_route(route)

########## optimization routine below ###########


if verbose: print("Starting optimization routine")

starting_fitness = routeset.compute_fitness(mode=fitness_mode)

# keep track of state
hasImprovement = True
iterations = 0
swaps = []

# write file with block routes before any swaps - used for animation of route changes
with open("output/animation/Animation_Before_" + day + "_mode_" + str(fitness_mode) + ".csv", "w") as text_file:
    for block in blocks.values():
        print("%s, %s" % (block.id, block.route), file=text_file)

#  keep going until no swap results in an improvement
while hasImprovement and iterations < maxIt:
    iterations += 1
    if verbose: print("Iteration: %s" % iterations)
    current_fitness = routeset.compute_fitness(mode=fitness_mode)
    best_fitness = current_fitness  # the best fitness we've found so far
    best_block_to_swap = None  # the best swap candidate
    best_block_target_route = None  # the route to which we swap the best swap candidate
    # iterate over all possible swaps (boundary blocks)
    for route in routeset.routes:
        for block_id in list(route.blocks.keys()):
            block = route.blocks[block_id]
            # find all neighboring blocks on the same day
            neighbors = filter(lambda x: x.day == block.day, block.get_adjacent_blocks(blocks))
            # find neighboring routes to this block
            neighboring_routes = set(block.route for block in neighbors) - set([block.route])
            components_before = route.count_components(blocks)
            for swap_candidate in neighboring_routes:
                # trial the swap
                routeset.swap_block(block, routes[swap_candidate])
                score = routeset.compute_fitness(mode=fitness_mode)
                components_after = routeset.get_route_by_id(route.id).count_components(blocks)

                if score < best_fitness and components_after <= components_before:
                    best_fitness = score
                    best_block_to_swap = block
                    best_block_target_route = routes[swap_candidate]

                # undo the swap
                routeset.swap_block(block, route)

    hasImprovement = best_block_to_swap is not None
    if hasImprovement:
        if verbose: print("swapping block " + best_block_to_swap.id + " from route " + best_block_to_swap.route + " to " + best_block_target_route.id)

        routeset.swap_block(best_block_to_swap, best_block_target_route)
        if verbose: print("Fitness before: " + str(current_fitness))
        if verbose: print("Fitness after swap: " + str(routeset.compute_fitness(mode=fitness_mode)))
        swaps.append( (best_block_to_swap.id, best_block_to_swap.route) )


if verbose: print("Terminated after %s iterations" % iterations)
ending_fitness = routeset.compute_fitness(mode=fitness_mode)
if verbose: print("Fitness before routine: %s" % starting_fitness)
if verbose: print("Fitness after routine: %s" % ending_fitness)

# writes customer master list for this result
with open("output/Output_" + day + "_mode_" + str(fitness_mode) + ".csv", "w") as text_file:
    print(routeset.to_csv(), file=text_file)

# writes list of blocks with their new route id for this result - can be used to visualize new routes
with open("output/Output_Blocks_" + day + "_mode_" + str(fitness_mode) + ".csv", "w") as text_file:
    print(routeset.blocks_to_csv(), file=text_file)

# writes sequential list of swaps made during the routine - useful for animating the results
with open("output/animation/Animation_Swaps_" + day + "_mode_" + str(fitness_mode) + ".csv", "w") as text_file:
    for swap in swaps:
        print("%s, %s" % (swap[0], swap[1]), file=text_file)

# writes out a txt file describing the improvement in the fitness function before and after
with open("output/Improvement_" + day + "_mode_" + str(fitness_mode) + ".txt", "w") as text_file:
    print("Start: %s\r\nEnd %s\r\nIterations: %s" % (starting_fitness, ending_fitness, iterations), file=text_file)
